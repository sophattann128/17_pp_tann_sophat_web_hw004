import InputForm from './components/InputForm';
import SweetAlert from './components/SweetAlert';
// import './App.css';

function App() {
  return (
    <div class="bg-green-100 h-screen">
      <header className="App-header">
       <InputForm/>
      </header>
    </div>
  );
}

export default App;
