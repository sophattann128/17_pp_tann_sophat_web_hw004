import React, { Component } from 'react'
import { DisplayTable } from './DisplayTable';

export class InputForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            studentObject: [],
            formData: {
                username: '',
                age: '',
                email: '',
                status: "Pending"
            }


        }
        this.handleChangeUserName = this.handleChangeUserName.bind(this);
        this.handleChangeAge = this.handleChangeAge.bind(this);
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    getId = (id) => {
        let value = this.state.studentObject
        value.map((student) => {
            if(student.id === id) {
                student.status = student.status === "Pending"? "Done" : "Pending";
            }
            console.log(student.id)
        });
        this.setState({value});


    }
    isEmpty(value) {
        return (value == null || (typeof value === "string" && value.trim().length === 0));
    }
    handleChangeUserName(event) {
        const value = event.target.value;
        this.setState(() => ({ formData: { ...this.state.formData, username: value } }),
            () => console.log(this.state.formData));

    }

    handleChangeAge(event) {
        const value = event.target.value;
        this.setState(() => ({ formData: { ...this.state.formData, age: value } }),
            () => { console.log(this.state.formData) });
    }

    handleChangeEmail(event) {
        const value = event.target.value;
        this.setState(() => ({ formData: { ...this.state.formData, email: value } }),
            () => { console.log(event.target.value == '') });
    }
    handleSubmit(event) {
        event.preventDefault();
        const newStudent = {
            id: this.state.studentObject.length + 1,
            username: this.isEmpty(this.state.formData.username) ? "null" : this.state.formData.username,
            age: this.isEmpty(this.state.formData.age) ? "null" : this.state.formData.age,
            email: this.isEmpty(this.state.formData.email) ? "null" : this.state.formData.email,
            status: this.state.formData.status
        }
        this.setState({
            studentObject: [...this.state.studentObject, newStudent]
        })




    }


    render() {
        return (
            <div class='flex justify-center flex-col items-center'>
                <div class="container bg-orange-300 p-6 w-[70%]  m-20 border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
                    <div className="flex flex-col justify-center items-center text-4xl font-bold">
                        <h1><span className='font-extrabold text-transparent bg-clip-text bg-gradient-to-r from-green-500 to-purple-500'>Please fill</span>  your information</h1>
                    </div>

                    <form onSubmit={this.handleSubmit}>

                        <label for="email-address-icon" className="block my-2 text-sm font-medium text-gray-900 dark:text-white">Your Email</label>
                        <div className="relative">
                            <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                                <svg aria-hidden="true" className="w-5 h-5 text-gray-500 dark:text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z"></path><path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z"></path></svg>
                            </div>
                            <input onChange={this.handleChangeEmail} value={this.state.formData.email} type="text" id="email-address-icon" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full pl-10 p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="name@example.com" />
                        </div>

                        <label for="website-admin" className="block my-2 text-sm font-medium text-gray-900 dark:text-white">Username</label>
                        <div className="flex">
                            <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                                @
                            </span>
                            <input onChange={this.handleChangeUserName} type="text" id="website-admin" className="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Sophat" />
                        </div>

                        <label for="website-admin" className="block my-2 text-sm font-medium text-gray-900 dark:text-white">Age</label>
                        <div className="flex">
                            <span className="inline-flex items-center px-3 text-sm text-gray-900 bg-gray-200 border border-r-0 border-gray-300 rounded-l-md dark:bg-gray-600 dark:text-gray-400 dark:border-gray-600">
                                ❤️
                            </span>
                            <input onChange={this.handleChangeAge} type="text" id="website-admin" className="rounded-none rounded-r-lg bg-gray-50 border border-gray-300 text-gray-900 focus:ring-blue-500 focus:border-blue-500 block flex-1 min-w-0 w-full text-sm p-2.5  dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="20" />
                        </div>
                        <div className="flex justify-between items-center flex-col mt-5">
                            <button type="submit" className="text-white bg-blue-700 font-bold hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Register</button>

                        </div>

                    </form>
                </div>
                <DisplayTable data={this.state.studentObject} giveId={this.getId} />

            </div>
        )
    }
}

export default InputForm