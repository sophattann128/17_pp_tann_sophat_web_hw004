import React, { Component } from 'react'
import './table.css';
import { Button } from './Button';
import SweetAlert from './SweetAlert';

export class DisplayTable extends Component {
    

    render() {
        return (
            <>
                <div className="container mb-20 w-[70%] flex justify-center al relative overflow-x-auto shadow-md sm:rounded-lg">
                    <table className="w-full text-sm text-left text-black font-bold dark:text-blue-100">
                        <thead className="text-lg font-bold bg-white uppercase  dark:text-white">
                            <tr>
                                <th scope="col" className="px-6 py-3">
                                    ID
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Email
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Username
                                </th>
                                <th scope="col" className="px-6 py-3">
                                    Age
                                </th>
                                <th className="px-20 py-3">
                                    Action
                                </th>

                            </tr>
                        </thead>
                        <tbody>

                            {this.props.data.map((item, index) => (
                                <tr key={index} className="bg-blue-500 text-white border-b border-blue-400">
                                    <th scope="row" className="px-6 py-4  whitespace-nowrap ">{item.id}</th>
                                    <td className="px-6 py-4">{item.email} </td>
                                    <td className="px-6 py-4">{item.username}</td>
                                    <td className="px-6 py-4">{item.age}</td>
                                    <td className="px-6 py-4">
                                        <Button handleClick = {()=> this.props.giveId(item.id)} value={item.status} color={item.status === "Pending"? "red": "green"}/>
                                        <SweetAlert id={item.id} username={item.username} age={item.age} email={item.email} />

                                    </td>

                                </tr>

                            ))}



                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}

export default DisplayTable