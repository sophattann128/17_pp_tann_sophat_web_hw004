import React, { Component } from 'react'

export class Button extends Component {
  

  render() {
    return (
      <button className='rounded-lg text-sm px-5 py-2.5 text-center mr-2 font-bold' onClick={this.props.handleClick} style={{backgroundColor: this.props.color}}>{this.props.value}</button>
    )
  }
}

export default Button