import React, { Component } from "react";
import Swal from "sweetalert2";
import { Button } from '../../../17_pp_tann_sophat_web_hw004/src/components/Button';

export default class SweetAlert extends Component {

    constructor() {
        super();
        this.HandleClick = this.HandleClick.bind(this);
    }

    HandleClick() {
        Swal.fire({
            title: "ID: " + this.props.id + "\n" +"Username: "+ this.props.username + "\n" +"Age: "+ this.props.age + "\n" +"Email: "+ this.props.email,
            type: 'success',
            
        });
    }
    


    render() {
        return (
            <Button handleClick = {this.HandleClick} value="Show more" color="blue"/>

        );
    }
}    